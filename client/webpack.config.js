const path = require("path");
const webpack = require("webpack");
const Dotenv = require("dotenv-webpack");

module.exports = {
  entry: ["./app/src/main.js"],
  output: {
    path: path.join(__dirname, "dist"),
    filename: "build.js",
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        loader: "babel-loader",
        exclude: /node_modules/,
        query: {
          presets: ["env"],
        },
      },
      {
        test: /\.scss$/,
        use: ["css-loader", "sass-loader"],
      },
      {
        test: /\.vue$/,
        loader: "vue-loader",
        options: {
          loaders: {
            scss: ["vue-style-loader", "css-loader", "sass-loader"],
            js: "babel-loader",
          },
        },
      },
    ],
  },
  node: {
    net: "empty",
    tls: "empty",
    dns: "empty",
    fs: "empty",
  },

  resolve: {
    alias: {
      vue: "vue/dist/vue.js",
      jquery: path.resolve(__dirname, "node_modules/jquery/dist/jquery.js"),
    },
  },
  stats: { colors: true },
  devtool: "cheap-module-source-map",
  plugins: [
    new webpack.ProvidePlugin({
      $: "jquery",
      jquery: "jquery",
      "window.jQuery": "jquery",
      jQuery: "jquery",
    }),
    new webpack.DefinePlugin({
      "process.env": {
        NODE_ENV: JSON.stringify(process.env.ENV),
      },
      API_URL: JSON.stringify(process.env.API_URL) || "http://localhost:4242",
      KEY_API_GOOGLE: JSON.stringify(process.env.KEY_API_GOOGLE),
      API_TOKEN: JSON.stringify(process.env.API_TOKEN) || "yolo",
    }),
    new Dotenv({
      path: "../.env",
    }),
  ],
};
