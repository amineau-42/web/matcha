"use strict";

const nodemailer = require("nodemailer");
const url = require("url");

module.exports = class Mailer {
  constructor() {
    this._signature = "<h2>L'équipe Matcha</h2>";
    this._sendMail = (mailOptions) => {
      return new Promise((resolve, reject) => {
        const transporter = nodemailer.createTransport({
          service: global.gConfig.smtp.service,
          auth: {
            user: global.gConfig.smtp.user,
            pass: global.gConfig.smtp.pass,
          },
        });
        transporter.sendMail(mailOptions, (error, info) => {
          if (error) {
            return reject(error);
          }
          console.log("message send :" + info.response);
          resolve();
        });
      });
    };
  }

  ForgotPassword(value) {
    const mailOptions = {
      from: global.gConfig.smtp.noreply,
      to: value.email,
      subject: "Changement de mot de passe",
      html: `<h5>Bonjour ${value.firstName}</h5>
    	  	<p>Vous pouvez dès à présent modifier votre mot de passe en suivant ce
            <a href="${[
              global.gConfig.client.url,
              "changepassword",
              value.linkPassword,
            ].reduce(url.resolve)}">
              lien
            </a>
          </p>
    	  	${this._signature}.`,
    };
    return this._sendMail(mailOptions);
  }
};
