"use strict";

const dotenv = require("dotenv").config();
const config = require("./config/config");
const express = require("express");
let app = express();
const http = require("http");
const https = require("https");
const cors = require("cors");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const passport = require("./passport");
const Mailer = require("./models/mailer");
const Queries = require("./queries");
const fs = require("fs");
const rfs = require("rotating-file-stream");
let server = undefined;

if (global.gConfig.node_env == "development") {
  if (dotenv.error) {
    console.log("No local env file loaded");
  }
}

let accessLogStream = rfs.createStream("access.log", {
  interval: "1d",
  path: global.gConfig.logs_path,
});

app.use(
  morgan("short", {
    skip: (req, res) => {
      return res.statusCode < 400;
    },
  })
);

app.use(morgan("common", { stream: accessLogStream }));

app
  .use(
    cors({
      origin: global.gConfig.allowed_hosts,
      methods: ["GET", "HEAD", "OPTIONS", "POST", "PUT", "DELETE"],
      allowedHeaders: [
        "Origin",
        "Accept",
        "x-forwarded-for",
        "matcha-token",
        "Content-Type",
      ],
    })
  )
  .use(bodyParser.json({ limit: "50mb" }))
  .use(bodyParser.urlencoded({ limit: "50mb", extended: false }))
  .use(passport.initialize())
  .use(passport.session());

let users = {};

if (global.gConfig.ssl) {
  server = https.createServer(
    {
      key: fs.readFileSync(global.gConfig.ssl_key_path),
      cert: fs.readFileSync(global.gConfig.ssl_crt_path),
    },
    app
  );
} else {
  server = http.createServer(app);
}

let io = require("socket.io")(server);

// move in socket.js
io.on("connection", (socket) => {
  socket.on("online", (id) => {
    users[socket.id] = { id, status: 1 };
    io.emit("user", users);
  });

  socket.on("logout", () => {
    const id = users[socket.id];
    delete users[socket.id];
    io.emit("user", users);
  });

  socket.on("focus off", (id) => {
    if (!users[socket.id]) return;
    users[socket.id] = { id, status: 2 };
    io.emit("user", users);
  });

  socket.on("disconnect", () => {
    if (!users[socket.id]) return;
    const id = users[socket.id];
    delete users[socket.id];
    io.emit("user", users);
  });
});

app.set("query", new Queries());
app.set("io", io);
app.set("mailer", new Mailer());
app.set("users", users);

//Routes
require("./routes/user")(app);
require("./routes/tags")(app);
require("./routes/pics")(app);
require("./routes/connexion")(app);
require("./routes/notif")(app);
require("./routes/chat")(app);
require("./routes/online")(app);
require("./routes/generate")(app);

server.listen(global.gConfig.port);
console.log(`Server starting in ${global.gConfig.host}:${global.gConfig.port}`);
