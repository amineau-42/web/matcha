"use strict";

const axios = require("axios");

exports.doDatabaseOperation = (query, params) => {
  return new Promise(function (resolve, reject) {
    if (params.id) params.id = Number(params.id);
    params.now = Date.now();
    axios({
      method: "POST",
      url: global.gConfig.neo4j.url,
      data: {
        statements: [
          {
            statement: query,
            parameters: params,
            includeStats: true,
          },
        ],
      },
    })
      .then((res) => resolve(res.data))
      .catch((err) => reject({ error: err }));
  });
};
