"use strict";

global.gConfig = {
  node_env: process.env.NODE_ENV || "development",
  host: process.env.API_HOST || "localhost",
  port: process.env.API_PORT || 4242,
  allowed_hosts: process.env.ALLOWED_HOSTS || "*",
  client: {
    url: process.env.CLIENT_URL || "http://localhost:8080",
  },
  neo4j: {
    host: process.env.NEO4J_HOST || "localhost",
    port: process.env.NEO4J_HTTP_PORT || 7474,
    user: process.env.NEO4J_USER || "neo4j",
    pass: process.env.NEO4J_PASS,
    get url() {
      return `https://${this.user}:${this.pass}@${this.host}:${this.port}/db/data/transaction/commit`;
    },
  },
  mongo: {
    host: process.env.MONGO_HOST || "localhost",
    port: process.env.MONGO_PORT || 7474,
    name: process.env.MONGO_DBNAME || "mongo",
    user: process.env.MONGO_USER || "mongo",
    pass: process.env.MONGO_PASS,
    get url() {
      return `mongodb://${this.user}:${this.pass}@${this.host}:${this.port}?authMechanism=DEFAULT`;
    },
  },
  smtp: {
    service: process.env.SMTP_SERV || "mail.mineau.fr",
    user: process.env.SMTP_USER || "matcha@mineau.fr",
    pass: process.env.SMTP_pass,
    noreply: process.env.MAIL_NOREPLY || "noreply@matcha.fr",
  },
  media: process.env.MEDIA || "./app/media",
  logs_path: process.env.LOGS_PATH || "./app/logs",
  logs_file: "access.log",

  token: process.env.API_TOKEN || "yolo",
  token_exp: process.env.API_TOKEN_EXP || "1d",
  bcrypt_saltrounds: process.env.SALTROUNDS || 10,

  ssl: /(true|yes|y|on|1)/i.test(process.env.API_SSL) || false,
  ssl_crt_path: process.env.API_SSL_CRT_PATH,
  ssl_key_path: process.env.API_SSL_KEY_PATH,
};

console.log(`Config: ${JSON.stringify(global.gConfig, null, 4)}`);
