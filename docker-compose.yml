version: "3"
services:
  api:
    container_name: ${API_NAME}
    build:
      context: api
      args:
        - IMAGE=${API_IMAGE}
        - TAG=${API_TAG}
    command: npm start
    depends_on:
      - nginx
      - neo4j
      - mongo
    expose:
      - ${API_PORT}
    volumes:
      - ./nginx/ssl:/etc/ssl
      - media:/media:rw
      - logs:/logs:rw
    environment:
      - NODE_ENV=${ENV}
      - API_PORT=${API_PORT}
      - API_TOKEN=${API_TOKEN}
      - API_URL=${PROTOCOLE}://${API_SUBDOMAIN}.${DOMAIN}
      - CLIENT_URL=${PROTOCOLE}://${CLIENT_SUBDOMAIN}.${DOMAIN}
      - ALLOWED_HOSTS=${PROTOCOLE}://${CLIENT_SUBDOMAIN}.${DOMAIN}
      - NEO4J_HOST=neo4j
      - NEO4J_HTTP_PORT=${NEO4J_HTTP_PORT}
      - NEO4J_PASS=${NEO4J_PASS}
      - MONGO_HOST=mongo
      - MONGO_PORT=${MONGO_PORT}
      - MONGO_DBNAME=${MONGO_DBNAME}
      - MONGO_USER=${MONGO_USER}
      - MONGO_PASS=${MONGO_PASS}
      - SMTP_USER=${SMTP_USER}
      - SMTP_PASS=${SMTP_PASS}
      - SMTP_SERV=${SMTP_SERV}
      - MAIL_NOREPLY=${MAIL_NOREPLY}
      - LOGS_PATH=${LOGS_PATH}
      - MEDIA=${MEDIA}
      - API_SSL=${SSL}
      - API_SSL_CRT_PATH=/etc/ssl/certs/nginx-selfsigned.crt
      - API_SSL_KEY_PATH=/etc/ssl/private/nginx-selfsigned.key

  client:
    container_name: ${CLIENT_NAME}
    build:
      context: client
      args:
        - IMAGE=${CLIENT_IMAGE}
        - TAG=${CLIENT_TAG}
    command: npm run build
    expose:
      - ${CLIENT_PORT}
    volumes:
      - static:/static:ro
      - client:/app:rw
    environment:
      - API_URL=${PROTOCOLE}://${API_SUBDOMAIN}.${DOMAIN}
      - API_TOKEN=${API_TOKEN}
      - KEY_API_GOOGLE=${KEY_API_GOOGLE}
    depends_on:
      - nginx

  neo4j:
    container_name: ${NEO4J_NAME}
    image: neo4j:3.5
    restart: on-failure
    environment:
      - NEO4J_AUTH=neo4j/${NEO4J_PASS}
      # - NEO4J_dbms_ssl_policy_https_enabled=true
    # ports:
    # - ${NEO4J_BOLT_PORT}:7687
    # - ${NEO4J_HTTP_PORT}:7474
    depends_on:
      - nginx
    volumes:
      - db_neo4j:/data:rw
      # - ./nginx/ssl/certs/nginx-selfsigned.crt:/ssl/neo4j.cert:ro
      # - ./nginx/ssl/private/nginx-selfsigned.key:/ssl/neo4j.key:ro

  mongo:
    container_name: ${MONGO_NAME}
    image: mongo
    restart: on-failure
    environment:
      - MONGO_INITDB_ROOT_USERNAME=${MONGO_USER}
      - MONGO_INITDB_ROOT_PASSWORD=${MONGO_PASS}
    volumes:
      - db_mongo:/data/db:rw

  mongo-express:
    container_name: ${MONGO_EXPRESS_NAME}
    image: mongo-express
    restart: on-failure
    depends_on:
      - mongo
      - nginx
    ports:
      - ${MONGO_EXPRESS_PORT}:8081
    volumes:
      - ./nginx/ssl:/etc/ssl
    environment:
      - ME_CONFIG_MONGODB_ADMINUSERNAME=${MONGO_USER}
      - ME_CONFIG_MONGODB_ADMINPASSWORD=${MONGO_PASS}
      - ME_CONFIG_BASICAUTH_USERNAME=${MONGO_EXPRESS_USER}
      - ME_CONFIG_BASICAUTH_PASSWORD=${MONGO_EXPRESS_PASS}
      - ME_CONFIG_MONGODB_PORT=${MONGO_PORT}
      - ME_CONFIG_SITE_SSL_ENABLED=${SSL}
      - ME_CONFIG_SITE_SSL_CRT_PATH=/etc/ssl/certs/nginx-selfsigned.crt
      - ME_CONFIG_SITE_SSL_KEY_PATH=/etc/ssl/private/nginx-selfsigned.key

  nginx:
    container_name: ${NGINX_NAME}
    image: nginx
    restart: on-failure
    ports:
      - 80:80
      - 443:443
      - ${NEO4J_BOLT_PORT}:${NEO4J_BOLT_PORT}
    environment:
      - NGINX_HTTP_PORT=${NGINX_HTTP_PORT}
      - NGINX_HTTPS_PORT=${NGINX_HTTPS_PORT}
      - API_NAME=${API_NAME}
      - NEO4J_NAME=${NEO4J_NAME}
      - MONGO_EXPRESS_NAME=${MONGO_EXPRESS_NAME}
      - API_PORT=${API_PORT}
      - NEO4J_HTTP_PORT=${NEO4J_HTTP_PORT}
      - NEO4J_HTTPS_PORT=${NEO4J_HTTPS_PORT}
      - NEO4J_BOLT_PORT=${NEO4J_BOLT_PORT}
      - MONGO_EXPRESS_PORT=${MONGO_EXPRESS_PORT}
      - API_URL=${API_SUBDOMAIN}.${DOMAIN}
      - CLIENT_URL=${CLIENT_SUBDOMAIN}.${DOMAIN}
      - MONGO_URL=${MONGO_SUBDOMAIN}.${DOMAIN}
      - NEO4J_URL=${NEO4J_SUBDOMAIN}.${DOMAIN}
    volumes:
      - ./nginx/nginx.conf:/tmp/nginx.conf
      - ./nginx/ssl:/etc/ssl
      - static:/var/www/static/:ro
      - client:/var/www/client/:ro
    command: /bin/bash -c "cat /tmp/nginx.conf | envsubst \" `printf '$${%s} ' $$(env | cut -f 1 -d =)`\" > /etc/nginx/nginx.conf && exec nginx -g 'daemon off;'"

volumes:
  client:
  static:
  media:
  logs:
  db_neo4j:
  db_mongo:
